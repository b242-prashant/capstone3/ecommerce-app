import { Fragment } from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';



export default function Home(){
    const data = {
        title: "E- Commerce Website",
        content: "Best Products Available",
        destination: "/products",
        label: "Start Shopping!"
    }

    return (
    
        <Fragment>
            <Banner data={data}/>
            <Highlights />
        </Fragment>
       
    )
}
