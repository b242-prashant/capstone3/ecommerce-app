import{ Row, Col, Card } from 'react-bootstrap';
export default function Hightlights() {
	return (
		<Row>

	
			<Col xs={12} md={4}>
				   <Card className="cardHighlight mt-3 p-3">
			      	<Card.Body>
			        <Card.Title>
			        	<h2> Amazing products with huge discounts </h2>
			        </Card.Title>
			        
			        <Card.Text>
			          Up to 50% off | Kitchen appliances, cookware & more
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>

			<Col xs={12} md={4}>
				   <Card className="cardHighlight mt-3 p-3">
			      	<Card.Body>
			        <Card.Title>
			        	<h2> Best quality products </h2>
			        </Card.Title>
	
			        <Card.Text>
			          Up to 50% off | Electronics like Laptop, Television, Phone.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>

			<Col xs={12} md={4}>
				   <Card className="cardHighlight mt-3 p-3">
			      	<Card.Body>
			        <Card.Title>
			        	<h2> Sign in for a better experience </h2>
			        </Card.Title>
			        
			        <Card.Text>
			          By signing in you will be able to order products which you like in our website.
			        </Card.Text>
			      </Card.Body>
			    </Card>
			</Col>
		</Row>
	)
}