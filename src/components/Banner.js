
import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({data}) {

	console.log(data);
    const {title, content, destination, label} = data;

    return (
        <Row>
            <Col>
                <h1>{title}</h1>
                <p>{content}</p>
                <Link to={destination} className="button2 mb-2 pb-2">{label}</Link>
            </Col>
        </Row>
    )
}