import PropTypes from 'prop-types';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
// import { useState, useEffect } from 'react';
import phone from '../images/phone.jpg';
import laptop from '../images/laptop.png';
import diary from '../images/diary.jpg';
import mouse from '../images/mouse.jpg';


export default function ProductsCard({productsProp}) {


	
	console.log(productsProp);

    let image;

	const { _id, name, description, price } = productsProp;

    // if(name===phone){
    //     let image = phone;
    // }
        (name === "Phone") && (image=phone );
        (name === "Laptop") && (image=laptop);
        (name === "Diary") && (image=diary); 
        (name === "Mouse") && (image=mouse); 


    return (
        <Container className="mt-5">
            <Row>
                
                <Col  lg={{ span: 10, offset: 1 }}>
                 
                    <Card className="ycar">
                        <Card.Body>
                            <Card.Img className="images" variant="top" src={image} />
                            <div className="cards">
                            <Card.Title>{name}</Card.Title>
                            <Card.Subtitle>Description:</Card.Subtitle>
                            <Card.Text>{description}</Card.Text>
                            <Card.Subtitle>Price:</Card.Subtitle>
                            <Card.Text>Rs. {price}</Card.Text>
                            <Link className="btn btn-primary" to={`/products/${_id}`}>Details</Link>
                            </div>
                        </Card.Body>
                    </Card>
                    
                </Col>
            </Row>
        </Container>
    )
}

ProductsCard.propTypes = {
    // The "shape" method is used to check if a prop object conforms to a specific "shape"
   products : PropTypes.shape({
        // Define the properties and their expected types
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired
    })
}
